<?php
// @codingStandardsIgnoreFile
// @codeCoverageIgnoreStart
$namespace = 'cariclub';
$rootpath = 'src/';
$fileinfos = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootpath)
);
$files = [];
foreach($fileinfos as $pathname => $fileinfo) {
    if (!$fileinfo->isFile()) continue;
    $path = str_replace($rootpath, '', $pathname);
    $parts = rtrim($path, '.php');
    $parts = ltrim($parts, '/');
    $parts = str_replace('.', '', $parts);
    $parts = str_replace('.', '', $parts);

    $explode = explode('/', $parts);
    if (count($explode) === 3) {
        $parts = str_replace($explode[1].'/', '', $parts);
    }

    $parts = str_replace('config/', $namespace .'\\'.'\\', $parts);
    if ($parts == 'config') {
        $parts = 'cariclub\\\config';
    }

    $files[$parts] = '/src/' . $path;
}

spl_autoload_register(
    function($class) {
        static $classes = null;
        if ($classes === null) {
            $classes = $files;
        }

        $cn = strtolower($class);
        if (isset($classes[$cn])) {
            require __DIR__ . $classes[$cn];
        }
    },
    true,
    false
);
// @codeCoverageIgnoreEnd
