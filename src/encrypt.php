<?php
//php vendor/CharityRootInc/config/src/encrypt.php /var/www/config THE_KEY THE_VALUE
require_once('Config.php');
define('_CONFIG_DIR_', $argv[1]);
echo PHP_EOL . 'Copy and paste the following into your config file: ' . PHP_EOL;
echo $argv[2] . ' = "' . \CharityRoot\Config::buildEncryptedKey($argv[3]) . '"' . PHP_EOL . PHP_EOL;

